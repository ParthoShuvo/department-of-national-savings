package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "contact_us_headOffice")
public class ContactUsHeadOffice extends ContactUsRegionalOffice {

    @Column(name = "Phone Number")
    private String phoneNumber;

    @Column(name = "Email Address")
    private String emailAddress;

    public ContactUsHeadOffice() {
        super();
    }

    public ContactUsHeadOffice(String designation, String telephone, String emailAddress) {
        super(designation);
        this.phoneNumber = telephone;
        this.emailAddress = emailAddress;
        this.save();
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public static List<ContactUsHeadOffice> getContactUsHeadOfficeList() {
        List<ContactUsHeadOffice> contactUsHeadOfficeList = new Select()
                .from(ContactUsHeadOffice.class)
                .execute();
        return contactUsHeadOfficeList;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
