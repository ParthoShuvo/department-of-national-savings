package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_family_table_2")
public class SavingsFamilyTable2 extends Savings3MonthsTable2{

    public SavingsFamilyTable2() {
        super();
    }

    public SavingsFamilyTable2(String timeOfPeriod, String rateOfProfit,
                               String SSP, String totalRate, String payableWithProfitPer100) {
        super(timeOfPeriod, rateOfProfit, SSP, totalRate, payableWithProfitPer100);
        this.save();
    }

    public static List<SavingsFamilyTable2> getSavingsFamilyTable2List()
    {
        List<SavingsFamilyTable2> savingsFamilyTable2List = new Select()
                .from(SavingsFamilyTable2.class)
                .execute();
        return savingsFamilyTable2List;
    }
}
