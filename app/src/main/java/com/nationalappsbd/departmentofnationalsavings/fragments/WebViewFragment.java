package com.nationalappsbd.departmentofnationalsavings.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.interfaces.Initializer;


/**
 * Created by Shuvojit Saha Shuvo on 1/26/2015.
 */
public class WebViewFragment extends Fragment implements Initializer {

    private Bundle bundle;
    private WebView webView;
    private WebSettings webSettings;
    private Context context;
    private View fragmentView;
    private String webUrl;
    //private UserNotifiedDialog userNotifiedDialog;
    /*private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            if (userNotifiedDialog.isShowing()) {
                userNotifiedDialog.closeDialog();
                Log.e(getClass().getName(), "web page has been loaded");
            }
        }
    };*/

    public WebViewFragment() {

    }

    public static WebViewFragment newInstance(String webUrl) {
        WebViewFragment webViewFragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("WebURL", webUrl);
        webViewFragment.setArguments(bundle);
        return webViewFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            bundle = getArguments();
            webUrl = bundle.getString("WebURL");
            context = getActivity();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentView = inflater.inflate(R.layout.web_view_fragment_layout, container, false);
        initialize();
        if (webView != null && webSettings != null && webUrl != null) {
            webSettings.setJavaScriptEnabled(true);
            setProgressDialog();
            setWebView(webUrl);
        }
        Log.e(getClass().getName(), "web view fragment called");

        return fragmentView;
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            webView = (WebView) fragmentView.findViewById(R.id.webView);
            webSettings = webView.getSettings();
        }
    }

    private void setProgressDialog() {
        String msg = context.getResources().getString(R.string.progressbar_loading_msg);
       /* userNotifiedDialog = new UserNotifiedDialog(context, "Loading Alert", msg);
        userNotifiedDialog.showDialog();*/
    }

    private void setWebView(String url) {
        webView.loadUrl(webUrl);
       // webView.setWebViewClient(webViewClient);
    }

}
