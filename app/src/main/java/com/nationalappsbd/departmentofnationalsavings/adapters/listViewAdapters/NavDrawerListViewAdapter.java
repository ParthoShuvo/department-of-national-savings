package com.nationalappsbd.departmentofnationalsavings.adapters.listViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.interfaces.Initializer;


/**
 * Created by shuvojit on 5/6/15.
 */
public class NavDrawerListViewAdapter extends BaseAdapter implements Initializer {

    private Context context;
    private String[] navDrawerMenuItemNames;
    private TextView txtNavListMenuItem;
    private View adapterView;
    private LayoutInflater layoutInflater;

    public NavDrawerListViewAdapter(Context context) {
        this.context = context;
        navDrawerMenuItemNames = context.getResources().getStringArray
                (R.array.navMenuItem);
    }


    @Override
    public int getCount() {
        return navDrawerMenuItemNames.length;
    }

    @Override
    public Object getItem(int position) {
        return navDrawerMenuItemNames[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            adapterView = layoutInflater.inflate(R.layout.nav_drawer_menu_item_layout,
                    null, false);
        } else {
            adapterView = convertView;
        }
        initialize();
        setMenuItemName(navDrawerMenuItemNames[position]);
        return adapterView;
    }

    private void setMenuItemName(String navDrawerMenuItemName) {
        if (txtNavListMenuItem != null) {
            txtNavListMenuItem.setText(navDrawerMenuItemName);
        }
    }



    @Override
    public void initialize() {
        if (adapterView != null) {
            txtNavListMenuItem = (TextView) adapterView.findViewById
                    (R.id.txt_nav_drawer_list_menu_name);
        }
    }
}
