package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "forms")
public class Forms extends IntroductionPdf implements Serializable{

    @Column(name = "word_link")
    private String wordLink;

    public Forms() {
        super();
    }

    public Forms(String title, String pdf, String wordLink) {
        super(title, pdf);
        this.wordLink = wordLink;
        this.save();
    }

    public String getWordLink() {
        return wordLink;
    }

    public void setWordLink(String wordLink) {
        this.wordLink = wordLink;
    }

    public static List<Forms> getFormsList() {
        List<Forms> formsList = new Select().from(Forms.class).execute();
        return formsList;
    }
}