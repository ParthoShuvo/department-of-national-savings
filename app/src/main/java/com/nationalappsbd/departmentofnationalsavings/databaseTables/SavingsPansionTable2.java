package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_pensioner_table_2")
public class SavingsPansionTable2 extends Savings3MonthsTable2 {

    public SavingsPansionTable2() {
        super();
    }

    public SavingsPansionTable2(String timeOfPeriod, String rateOfProfit,
                                String SSP, String totalRate, String payableWithProfitPer100) {
        super(timeOfPeriod, rateOfProfit, SSP, totalRate, payableWithProfitPer100);
        this.save();
    }

    public static List<SavingsPansionTable2> getSavingsPansionTable2List()
    {
        List<SavingsPansionTable2> savingsPansionTable2List = new Select()
                .from(SavingsPansionTable2.class)
                .execute();
        return savingsPansionTable2List;
    }
}
