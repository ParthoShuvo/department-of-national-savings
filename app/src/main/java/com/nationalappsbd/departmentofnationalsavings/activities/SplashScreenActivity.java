package com.nationalappsbd.departmentofnationalsavings.activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.infos.ConnectivityInfo;


public class SplashScreenActivity extends ActionBarActivity {

    private boolean backedScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_layout);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                backedScreen = true;
            }
        }, 5000);
        if (!ConnectivityInfo.isInternetConnectionOn(SplashScreenActivity.this)) {
            Toast.makeText(SplashScreenActivity.this,
                    "আপনার ইন্টারনেট সংযোগ বন্ধ", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (backedScreen) {
            finish();
        }
    }
}
