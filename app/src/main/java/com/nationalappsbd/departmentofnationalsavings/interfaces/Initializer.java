package com.nationalappsbd.departmentofnationalsavings.interfaces;

/**
 * Created by shuvojit on 5/6/15.
 */
public interface Initializer {

    public void initialize();
}
