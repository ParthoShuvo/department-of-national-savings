package com.nationalappsbd.departmentofnationalsavings.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.adapters.listViewAdapters.DMPInfoListViewAdapter;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.AboutUs;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.ContactUsHeadOffice;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.ContactUsRegionalOffice;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.ContactUsSpecialBureau;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Forms;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.IntroductionPdf;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.IntroductionTable;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.KeyFeatures;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.OrganogramAssociateAgencies;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.OrganogramChaterOfDuites;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings3MonthsTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings3MonthsTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings3MonthsTable3;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings5YearsTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings5YearsTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsFamilyTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsFamilyTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsFamilyTable3;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable3;

import java.util.List;


/**
 * Created by shuvojit on 5/7/15.
 */
public class DMPNecessaryInfoFragment extends Fragment {

    private ListView listView;
    private Context context;
    private int FRAGMENT_SHOW_TYPE;
    private LayoutInflater layoutInflater;
    private String FRAGMENT_NAME;
    private GridView gridView;


    public DMPNecessaryInfoFragment() {

    }

    public static Fragment getNewInstance(final int FRAGMENT_SHOW_TYPE,
                                          final String Fragment_Name) {
        Bundle bundle = new Bundle();
        bundle.putInt("FRAGMENT_SHOW_TYPE", FRAGMENT_SHOW_TYPE);
        bundle.putString("FRAGMENT_NAME", Fragment_Name);
        DMPNecessaryInfoFragment dmpNecessaryInfoFragment = new DMPNecessaryInfoFragment();
        dmpNecessaryInfoFragment.setArguments(bundle);
        return dmpNecessaryInfoFragment;
    }

   /* public static Fragment getNewInstance(final int FRAGMENT_SHOW_TYPE,
                                          final DMPUnitsTable dmpUnitsTableData) {
        Bundle bundle = new Bundle();
        bundle.putInt("FRAGMENT_SHOW_TYPE", FRAGMENT_SHOW_TYPE);
        bundle.putSerializable("DMP UNIT", dmpUnitsTableData);
        DMPNecessaryInfoFragment dmpNecessaryInfoFragment = new DMPNecessaryInfoFragment();
        dmpNecessaryInfoFragment.setArguments(bundle);
        return dmpNecessaryInfoFragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        layoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Bundle bundle = getArguments();
        FRAGMENT_SHOW_TYPE = bundle.getInt("FRAGMENT_SHOW_TYPE");
        FRAGMENT_NAME = bundle.getString("FRAGMENT_NAME");
        //dmpUnitsTableData = (DMPUnitsTable) bundle.getSerializable("DMP UNIT");

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = null;

        switch (FRAGMENT_SHOW_TYPE) {
            case 1:
            case 2:
            case 3:
            case 7:
            case 8:
            case 6:
            case 5:
            case 9:
            case 10:
            case 12:
            case 13:
                fragmentView = layoutInflater.inflate(R.layout.dmp_info_list_view_layout,
                        null, false);
                break;
            case 11:
                fragmentView = layoutInflater.inflate(R.layout.pic_image_horizontal_layout, null,
                        false);
                break;

        }
        setFragmentLayoutElements(fragmentView);
        return fragmentView;
    }

    private void setFragmentLayoutElements(View fragmentView) {
        switch (FRAGMENT_SHOW_TYPE) {
            case 1:
            case 2:
            case 7:
            case 3:
            case 8:
            case 6:
            case 5:
            case 9:
            case 10:
            case 12:
            case 13:
                listView = (ListView) fragmentView.findViewById(R.id.dmp_info_list_view);
                setListFragmentView(fragmentView);
                break;
            case 11:
                ImageView imageView = (ImageView) fragmentView.findViewById(R.id.imageView);
                imageView.setImageResource(R.drawable.folw_chat);
                break;

        }
    }

    private void setVerticallyImageRextFragmnetView(View fragmentView) {
        ImageView imageView = (ImageView) fragmentView.findViewById(R.id.imageView);
        TextView txtDMPINfo = (TextView) fragmentView.findViewById(R.id.dmp_text_info);
        /*DmpCommissionerTable dmpCommissioner = DmpCommissionerTable.getDmpCommissioner();
        String details = null;
        if (dmpCommissioner != null) {

            Picasso.with(context)
                    .load(dmpCommissioner
                            .getImageLink())
                    .placeholder(R.drawable.dmp_comissioner)
                    .error(R.drawable.dmp_comissioner)
                    .into(imageView);
            switch (FRAGMENT_NAME) {
                case "message":
                    details = dmpCommissioner.getMessage().toString().trim();
                    txtDMPINfo.setText(details);
                    break;
                case "biography":
                    details = dmpCommissioner.getBiography().toString().trim();
                    txtDMPINfo.setText(details);
                    break;
            }

        }*/

    }

    private void setTextFragmentView(View fragmentView) {
        TextView txtDMPInfo = (TextView) fragmentView.findViewById(R.id.txt_dmp_info);
       /* if (dmpUnitsTableData != null) {
            String details = dmpUnitsTableData.getDetails().toString().trim();
            txtDMPInfo.setText(details);
            Log.e(getClass().getName(), "found");
        }*/
    }

    private void setListFragmentView(View fragmentView) {
        DMPInfoListViewAdapter dmpInfoListViewAdapter = null;
        switch (FRAGMENT_NAME) {
            case "Introduction_pdf":
                List<IntroductionPdf> introductionPdfList = IntroductionPdf
                        .getIntroductionPdfList();
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter
                        (context, 2, introductionPdfList);
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                }

                break;
            case "Introduction_details":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 1,
                        IntroductionTable.getIntroductionTableList());
                if (dmpInfoListViewAdapter != null) {
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Formation":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 3,
                        AboutUs.getAboutUsList("Formation of Department National Savings (DNS) :"));
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Organizational Setup":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 4,
                        AboutUs.getAboutUsList("Organizational Setup"));
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Form":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 5,
                        Forms.getFormsList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "CHARTER_OF_DUTIES":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 6,
                        OrganogramChaterOfDuites.getOrganogramChaterOfDuitesList());
                Log.e(getClass().getName(),
                        OrganogramChaterOfDuites.getOrganogramChaterOfDuitesList().size() + "");
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Associate_Agencies":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 7,
                        OrganogramAssociateAgencies.getOrganogramAssociateAgenciesList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Head Office":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 8,
                        ContactUsHeadOffice.getContactUsHeadOfficeList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Regional Office":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 9,
                        ContactUsRegionalOffice.getContactUsRegionalOfficeList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Special Bureau":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 10,
                        ContactUsSpecialBureau.getContactUsSpecialBureauList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Key Features":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 11,
                        KeyFeatures.getKeyFeaturesList());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Sanchayapatra":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 12,
                        context.getResources().getStringArray(R.array.sanchayapatra));
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Development Bond":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 13,
                        context.getResources().getStringArray(R.array.developmentBond));
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "5 years 1":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 14,
                        Savings5YearsTable1.savings5YearsTable1List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "5 years 2":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 15,
                        Savings5YearsTable2.getSavings5YearsTable2List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "3 months 1":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 16,
                        Savings3MonthsTable1.getSavings3MonthsTable1List());
                Log.e(getClass().getName(), Savings3MonthsTable1
                        .getSavings3MonthsTable1List().size() + "");
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "3 months 2":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 17,
                        Savings3MonthsTable2.getSavings3MonthsTable2List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "3 months 3":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 18,
                        Savings3MonthsTable3.getSavings3MonthsTable3List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Pansion 1":
              List<SavingsPansionTable1> savingsPansionTable1List = SavingsPansionTable1
                        .getSavingsPansionTable1List();
                Log.e(getClass().getName(), savingsPansionTable1List.size() + "");
                /*dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 19,
                        SavingsPansionTable1.getSavingsPansionTable1List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }*/
                break;
            case "Pansion 2":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 20,
                        SavingsPansionTable2.getSavingsPansionTable2List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Pansion 3":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 21,
                        SavingsPansionTable3.getSavingsPansionTable3List());
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Family 1":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 22,
                        SavingsFamilyTable1.getSavingsFamilyTable1List());
                Log.e(getClass().getName(), Savings3MonthsTable1
                        .getSavings3MonthsTable1List().size() + "");
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Family 2":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 23,
                        SavingsFamilyTable2.getSavingsFamilyTable2List());
                Log.e(getClass().getName(), Savings3MonthsTable1
                        .getSavings3MonthsTable1List().size() + "");
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
            case "Family 3":
                dmpInfoListViewAdapter = new DMPInfoListViewAdapter(context, 24,
                        SavingsFamilyTable3.getFamilyTable3List());
                Log.e(getClass().getName(), Savings3MonthsTable1
                        .getSavings3MonthsTable1List().size() + "");
                if (dmpInfoListViewAdapter != null) {
                    Log.e(getClass().getName(), "Not Null");
                    listView.setAdapter(dmpInfoListViewAdapter);
                }
                break;
        }

    }

}
