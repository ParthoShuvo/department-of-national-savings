package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/8/15.
 */

@Table(name = "keyFeatures")
public class KeyFeatures extends Model {

    @Column(name = "Name of\n" + "Schemes")
    private String nameOfSchemes;

    @Column(name = "Maturity")
    private String maturity;

    @Column(name = "limits")
    private String limit;

    @Column(name = "Interest rate")
    private String interestRate;

    @Column(name = "Tax\n" +
            "Treatment")
    private String taxTreatment;

    @Column(name = "Investors")
    private String investors;

    @Column(name = "Penalty")
    private String penalty;

    @Column(name = "Implementing\n" +
            "agency")
    private String implementingAgency;

    @Column(name = "Comments")
    private String comments;

    public KeyFeatures() {
        super();
    }

    public KeyFeatures(String nameOfSchemes, String maturity, String limit,
                       String interestRate, String taxTreatment, String investors,
                       String penalty, String implementingAgency, String comments) {
        super();
        this.nameOfSchemes = nameOfSchemes;
        this.maturity = maturity;
        this.limit = limit;
        this.interestRate = interestRate;
        this.taxTreatment = taxTreatment;
        this.investors = investors;
        this.penalty = penalty;
        this.implementingAgency = implementingAgency;
        this.comments = comments;
        this.save();
    }

    public String getNameOfSchemes() {
        return nameOfSchemes;
    }

    public void setNameOfSchemes(String nameOfSchemes) {
        this.nameOfSchemes = nameOfSchemes;
    }

    public String getMaturity() {
        return maturity;
    }

    public void setMaturity(String maturity) {
        this.maturity = maturity;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getTaxTreatment() {
        return taxTreatment;
    }

    public void setTaxTreatment(String taxTreatment) {
        this.taxTreatment = taxTreatment;
    }

    public String getInvestors() {
        return investors;
    }

    public void setInvestors(String investors) {
        this.investors = investors;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getImplementingAgency() {
        return implementingAgency;
    }

    public void setImplementingAgency(String implementingAgency) {
        this.implementingAgency = implementingAgency;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public static List<KeyFeatures> getKeyFeaturesList() {
        List<KeyFeatures> keyFeaturesList = new Select()
                .from(KeyFeatures.class)
                .execute();
        return keyFeaturesList;
    }
}
