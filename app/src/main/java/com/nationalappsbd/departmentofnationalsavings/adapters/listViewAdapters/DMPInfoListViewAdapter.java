package com.nationalappsbd.departmentofnationalsavings.adapters.listViewAdapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.activities.FragmentActivity;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.AboutUs;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.ContactUsHeadOffice;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.ContactUsRegionalOffice;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.ContactUsSpecialBureau;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Forms;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.IntroductionPdf;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.IntroductionTable;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.KeyFeatures;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.OrganogramAssociateAgencies;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.OrganogramChaterOfDuites;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings3MonthsTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings3MonthsTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings3MonthsTable3;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings5YearsTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.Savings5YearsTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsFamilyTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsFamilyTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsFamilyTable3;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable3;

import java.util.List;

/**
 * Created by shuvojit on 5/7/15.
 */
public class DMPInfoListViewAdapter extends BaseAdapter {
    private String[] categoriesName;
    private String[] detailsName;
    private int LIST_VIEW_SHOW_TYPE;
    private Context context;
    private LayoutInflater layoutInflater;
    private List<IntroductionPdf> introductionPdfList;
    private List<IntroductionTable> introductionTableList;
    private List<OrganogramChaterOfDuites> organogramChaterOfDuitesList;
    private List<OrganogramAssociateAgencies> organogramAssociateAgenciesList;
    private List<Forms> formsList;
    private List<AboutUs> aboutUsList;
    private List<ContactUsHeadOffice> contactUsHeadOfficeList;
    private List<ContactUsSpecialBureau> contactUsSpecialBureauList;
    private List<ContactUsRegionalOffice> contactUsRegionalOfficeList;
    private int[] images;
    private List<KeyFeatures> keyFeaturesList;
    private List<Savings5YearsTable2> savings5YearsTable2List;
    private List<Savings5YearsTable1> savings5YearsTable1List;
    private List<Savings3MonthsTable1> savings3MonthsTable1List;
    private List<Savings3MonthsTable2> savings3MonthsTable2List;
    private List<Savings3MonthsTable3> savings3MonthsTable3List;
    private List<SavingsPansionTable1> savingsPansionTable1List;
    private List<SavingsPansionTable2> savingsPansionTable2List;
    private List<SavingsPansionTable3> savingsPansionTable3List;
    private List<SavingsFamilyTable1> savingsFamilyTable1List;
    private List<SavingsFamilyTable2> savingsFamilyTable2List;
    private List<SavingsFamilyTable3> savingsFamilyTable3List;
   /* private List<WeRememberTable> weRememberTables;
    private List<ImportantNumbersTable> importantNumbersTableList;
    private List<DMPPressTable> dmpPressTableList;
    private List<ComplaintTable> complaintTableList;
    private List<DMPServicesTable> dmpServicesTableList;*/

    //LIST_VIEW_SHOW_TYPE = 1 (DMP Services)
    //LIST_VIEW_SHOW_TYPE = 2 (NEW INITIATIVES)
    //LIST_VIEW_SHOW_TYPE = 3 (DMP Partners)
    //LIST_VIEW_SHOW_TYPE = 4 (We remember)
    //LIST_VIEW_SHOW_TYPE = 5 (Emergency helpline)
    //LIST_VIEW_SHOW_TYPE = 6 (Extortion helpline)
    //LIST_VIEW_SHOW_TYPE = 7 (DMP Press)


    public DMPInfoListViewAdapter(Context context, int LIST_VIEW_SHOW_TYPE,
                                  String[] categoriesName) {

        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.LIST_VIEW_SHOW_TYPE = LIST_VIEW_SHOW_TYPE;
        this.categoriesName = categoriesName;
    }


    public DMPInfoListViewAdapter(Context context, int LIST_VIEW_SHOW_TYPE, List dmpInfoList) {
        this.context = context;
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.LIST_VIEW_SHOW_TYPE = LIST_VIEW_SHOW_TYPE;
        TypedArray typedArray = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                introductionTableList = dmpInfoList;
                break;
            case 2:
                introductionPdfList = dmpInfoList;
                break;
            case 3:
            case 4:
                aboutUsList = dmpInfoList;
                break;
            case 5:
                formsList = dmpInfoList;
                break;
            case 6:
                organogramChaterOfDuitesList = dmpInfoList;
                break;
            case 7:
                organogramAssociateAgenciesList = dmpInfoList;
                break;
            case 8:
                contactUsHeadOfficeList = dmpInfoList;
                break;
            case 9:
                contactUsRegionalOfficeList = dmpInfoList;
                break;
            case 10:
                contactUsSpecialBureauList = dmpInfoList;
                break;
            case 11:
                keyFeaturesList = dmpInfoList;
                break;
            case 14:
                savings5YearsTable1List = dmpInfoList;
                break;
            case 15:
                savings5YearsTable2List = dmpInfoList;
                break;
            case 16:
                savings3MonthsTable1List = dmpInfoList;
                break;
            case 17:
                savings3MonthsTable2List = dmpInfoList;
                break;
            case 18:
                savings3MonthsTable3List = dmpInfoList;
                break;
            case 19:
                savingsPansionTable1List = dmpInfoList;
                break;
            case 20:
                savingsPansionTable2List = dmpInfoList;
                break;
            case 21:
                savingsPansionTable3List = dmpInfoList;
                break;
            case 22:
                savingsFamilyTable1List = dmpInfoList;
                break;
            case 23:
                savingsFamilyTable2List = dmpInfoList;
                break;
            case 24:
                savingsFamilyTable3List = dmpInfoList;
                break;



        }
    }


    @Override
    public int getCount() {
        int count = 0;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                count = introductionTableList.size();
                break;
            case 2:
                count = introductionPdfList.size();
                break;
            case 3:
            case 4:
                count = aboutUsList.size();
                break;
            case 5:
                count = formsList.size();
                break;
            case 6:
                count = organogramChaterOfDuitesList.size();
                break;
            case 7:
                count = organogramAssociateAgenciesList.size();
                break;
            case 8:
                count = contactUsHeadOfficeList.size();
                break;
            case 9:
                count = contactUsRegionalOfficeList.size();
                break;
            case 10:
                count = contactUsSpecialBureauList.size();
                break;
            case 11:
                count = keyFeaturesList.size();
                break;
            case 12:
            case 13:
                count = categoriesName.length;
                break;
            case 14:
                count = savings5YearsTable1List.size();
                break;
            case 15:
                count = savings5YearsTable2List.size();
                break;
            case 16:
                count = savings3MonthsTable1List.size();
                break;
            case 17:
                count = savings3MonthsTable2List.size();
                break;
            case 18:
                count = savings3MonthsTable3List.size();
                break;
            case 19:
                count = savingsPansionTable1List.size();
                break;
            case 20:
                count = savingsPansionTable2List.size();
                break;
            case 21:
                count = savingsPansionTable3List.size();
                break;
            case 22:
                count = savingsFamilyTable1List.size();
                break;
            case 23:
                count = savingsFamilyTable2List.size();
                break;
            case 24:
                count = savingsFamilyTable3List.size();
                break;

        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                object = introductionTableList.get(position);
                break;
            case 2:
                object = introductionPdfList.get(position);
                break;
            case 3:
            case 4:
                object = aboutUsList.get(position);
                break;
            case 5:
                object = formsList.get(position);
                break;
            case 6:
                object = organogramChaterOfDuitesList.get(position);
                break;
            case 7:
                object = organogramAssociateAgenciesList.get(position);
                break;
            case 8:
                object = contactUsHeadOfficeList.get(position);
                break;
            case 9:
                object = contactUsRegionalOfficeList.get(position);
                break;
            case 10:
                object = contactUsSpecialBureauList.get(position);
                break;
            case 11:
                object = keyFeaturesList.get(position);
                break;
            case 12:
            case 13:
                object = categoriesName[position];
                break;
            case 14:
                object = savings5YearsTable1List.get(position);
                break;
            case 15:
                object = savings5YearsTable2List.get(position);
                break;
            case 16:
                object = savings3MonthsTable1List.get(position);
                break;
            case 17:
                object = savings3MonthsTable2List.get(position);
                break;
            case 18:
                object = savings3MonthsTable3List.get(position);
                break;
            case 19:
                object = savingsPansionTable1List.get(position);
                break;
            case 20:
                object = savingsPansionTable2List.get(position);
                break;
            case 21:
                object = savingsPansionTable3List.get(position);
                break;
            case 22:
                object = savingsFamilyTable1List.get(position);
                break;
            case 23:
                object = savingsFamilyTable2List.get(position);
                break;
            case 24:
                object = savingsFamilyTable3List.get(position);
                break;

        }

        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View adapterView = null;
        if (convertView == null && layoutInflater != null) {
            switch (LIST_VIEW_SHOW_TYPE) {
                case 1:
                case 6:
                    adapterView = layoutInflater.inflate
                            (R.layout.category_details_layout, null, false);
                    break;
                case 2:
                    adapterView = layoutInflater.inflate
                            (R.layout.info_view_layout, null, false);
                    break;
                case 3:
                case 4:
                case 7:
                    adapterView = layoutInflater.inflate
                            (R.layout.dmp_necessary_info_layout, null, false);
                    break;
                case 5:
                    adapterView = layoutInflater.inflate
                            (R.layout.info_view_layout, null, false);
                    break;
                case 8:
                case 9:
                case 10:
                    adapterView = layoutInflater.inflate(R.layout.info_officers, null, false);
                    break;
                case 11:
                    adapterView = layoutInflater.inflate(R.layout.key_features_layout, null, false);
                    break;
                case 12:
                case 13:
                    adapterView = layoutInflater.inflate
                            (R.layout.info_view_layout, null, false);
                    break;
                case 14:
                    adapterView = layoutInflater.inflate(R.layout.category_details_layout,
                            null, false);
                    break;
                case 15:
                    adapterView = layoutInflater.inflate(R.layout.tk_info_layout, null, false);
                    break;
                case 16:
                case 19:
                case 22:
                    adapterView = layoutInflater.inflate(R.layout.category_details_layout,
                            null, false);
                    break;
                case 17:
                case 20:
                case 23:
                    adapterView = layoutInflater.inflate(R.layout.tk_info_layout, null, false);
                    break;
                case 18:
                case 21:
                case 24:
                    adapterView = layoutInflater.inflate(R.layout.layout_investment, null, false);
                    break;

            }
        } else {
            adapterView = convertView;
        }
        setRequiredFields(adapterView, position);
        return adapterView;
    }

    private void setRequiredFields(View adapterView, int position) {
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                setRequierdFieldsForDMPServices(adapterView, position);
                break;
            case 2:
                setRequiredFieldsForNewInitiatives(adapterView, position);
                break;
            case 3:
            case 4:
                setRequierdFieldsForDMPPartners(adapterView, position);
                break;
            case 5:
                setRequiredFieldsForForms(adapterView, position);
                break;
            case 6:
                setRequierdFieldsForCharterDuites(adapterView, position);
                break;
            case 7:
                setRequiredFieldsForAgencies(adapterView, position);
                break;
            case 8:
                setRequiredFieldsForContactUsHeadOffice(adapterView, position);
                break;
            case 9:
                setRequiredFieldsForContactUsRegionalOffices(adapterView, position);
                break;
            case 10:
                setRequierdFieldsForContactUsSpcialBureu(adapterView, position);
                break;
            case 11:
                setRequierdFieldsForKeyFeaturesList(adapterView, position);
                break;
            case 12:
            case 13:
                setRequiredFielsForSavingsProduct(adapterView, position);
                break;
            case 14:
            case 16:
            case 19:
            case 22:
                setRequiredFieldsForSavingsInfo(adapterView, position);
                break;
            case 15:
            case 17:
            case 20:
            case 23:
                setRequiredFieldsForSavingsTKInfo(adapterView, position);
                break;
            case 18:
            case 21:
            case 24:
                setRequiredFieldsForSavingsInvestment(adapterView, position);
                break;
           /* case 4:
                setRequierdFieldsForWeRemember(adapterView, position);
                break;
            case 5:
            case 6:
                setRequiredFieldsForImportantNumbers(adapterView, position);
                break;
            case 7:
                setRequiredFieldsForDMPPress(adapterView, position);
                break;
            case 8:
                setRequierdFieldsForComplaintNumbers(adapterView, position);
                break;*/

        }
    }

    private void setRequiredFieldsForSavingsInvestment(View adapterView, int position) {
        String timePeriod = null;
        String rateOfProfit = null;
        String SSP = null;
        String totalRate = null;
        TextView txtTimePeriod = (TextView) adapterView.findViewById(R.id.txtInvestmentAmount);
        TextView txtRateOfProfits = (TextView) adapterView.findViewById(R.id.txtAmountOfProfit);
        TextView txtSSP = (TextView) adapterView.findViewById(R.id.txtSSP);
        TextView txtTotalRate = (TextView) adapterView.findViewById(R.id.txtAmountOfTotalProfit);
        switch (LIST_VIEW_SHOW_TYPE)
        {
            case 18:
                Savings3MonthsTable3 savings3MonthsTable3 = savings3MonthsTable3List.get(position);
                timePeriod = getStringData(savings3MonthsTable3.getInvestmentAmount());
                rateOfProfit = getStringData(savings3MonthsTable3.getAmountOfProfit());
                SSP = getStringData(savings3MonthsTable3.getAmountOfSSP());
                totalRate = getStringData(savings3MonthsTable3.getAmountOfTotalProfit());
                break;
            case 21:
                SavingsPansionTable3 savingsPansionTable3 = savingsPansionTable3List.get(position);
                timePeriod = getStringData(savingsPansionTable3.getInvestmentAmount());
                rateOfProfit = getStringData(savingsPansionTable3.getAmountOfProfit());
                SSP = getStringData(savingsPansionTable3.getAmountOfSSP());
                totalRate = getStringData(savingsPansionTable3.getAmountOfTotalProfit());
                break;
            case 24:
                SavingsFamilyTable3 savingsFamilyTable3 = savingsFamilyTable3List.get(position);
                timePeriod = getStringData(savingsFamilyTable3.getInvestmentAmount());
                rateOfProfit = getStringData(savingsFamilyTable3.getAmountOfProfit());
                SSP = getStringData(savingsFamilyTable3.getAmountOfSSP());
                totalRate = getStringData(savingsFamilyTable3.getAmountOfTotalProfit());
                break;
        }
        txtTimePeriod.setText(timePeriod);
        txtRateOfProfits.setText(rateOfProfit);
        txtSSP.setText(SSP);
        txtTotalRate.setText(totalRate);

    }

    private void setRequiredFieldsForSavingsTKInfo(View adapterView, int position) {
        String timePeriod = null;
        String rateOfProfit = null;
        String SSP = null;
        String totalRate = null;
        String ProfitTK = null;
        TextView txtTimePeriod = (TextView) adapterView.findViewById(R.id.txtTimePeriod);
        TextView txtRateOfProfits = (TextView) adapterView.findViewById(R.id.txtRateOfProfit);
        TextView txtSSP = (TextView) adapterView.findViewById(R.id.txtSSP);
        TextView txtTotalRate = (TextView) adapterView.findViewById(R.id.txtTotaltRate);
        TextView txtProfitTk = (TextView) adapterView.findViewById(R.id.txtPayableWithProfit);
        switch (LIST_VIEW_SHOW_TYPE) {
            case 15:
                Savings5YearsTable2 savings5YearsTable2 = savings5YearsTable2List.get(position);
                timePeriod = getStringData(savings5YearsTable2.getTimeOfPeriod());
                rateOfProfit = getStringData(savings5YearsTable2.getRateOfProfit());
                SSP = getStringData(savings5YearsTable2.getSSP());
                totalRate = getStringData(savings5YearsTable2.getTotalRate());
                ProfitTK = getStringData(savings5YearsTable2.getPayableWithProfitPer100());
                break;
            case 17:
                Savings3MonthsTable2 savings3MonthsTable2 = savings3MonthsTable2List.get(position);
                timePeriod = getStringData(savings3MonthsTable2.getTimeOfPeriod());
                rateOfProfit = getStringData(savings3MonthsTable2.getRateOfProfit());
                SSP = getStringData(savings3MonthsTable2.getSSP());
                totalRate = getStringData(savings3MonthsTable2.getTotalRate());
                ProfitTK = getStringData(savings3MonthsTable2.getPayableWithProfitPer100());
                break;
            case 20:
                SavingsPansionTable2 savingsPansionTable2 = savingsPansionTable2List.get(position);
                timePeriod = getStringData(savingsPansionTable2.getTimeOfPeriod());
                rateOfProfit = getStringData(savingsPansionTable2.getRateOfProfit());
                SSP = getStringData(savingsPansionTable2.getSSP());
                totalRate = getStringData(savingsPansionTable2.getTotalRate());
                ProfitTK = getStringData(savingsPansionTable2.getPayableWithProfitPer100());
                break;
            case 21:
                SavingsFamilyTable2 savingsFamilyTable2 = savingsFamilyTable2List.get(position);
                timePeriod = getStringData(savingsFamilyTable2.getTimeOfPeriod());
                rateOfProfit = getStringData(savingsFamilyTable2.getRateOfProfit());
                SSP = getStringData(savingsFamilyTable2.getSSP());
                totalRate = getStringData(savingsFamilyTable2.getTotalRate());
                ProfitTK = getStringData(savingsFamilyTable2.getPayableWithProfitPer100());
                break;
        }
        txtTimePeriod.setText(timePeriod);
        txtRateOfProfits.setText(rateOfProfit);
        txtSSP.setText(SSP);
        txtTotalRate.setText(totalRate);
        txtProfitTk.setText(ProfitTK);
    }

    private void setRequiredFieldsForSavingsInfo(View adapterView, int position) {

        String category = null;
        String details = null;
        TextView txtCategoryName = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDetails = (TextView) adapterView.findViewById(R.id.txt_details);
        switch (LIST_VIEW_SHOW_TYPE) {
            case 14:
                Savings5YearsTable1 savings5YearsTable1 = savings5YearsTable1List.get(position);
                category = savings5YearsTable1.getTitle();
                details = savings5YearsTable1.getData();
                break;
            case 16:
                Savings3MonthsTable1 savings3MonthsTable1 = savings3MonthsTable1List.get(position);
                category = savings3MonthsTable1.getTitle();
                details = savings3MonthsTable1.getData();
                break;
            case 19:
                SavingsPansionTable1 savingsPansionTable1 = savingsPansionTable1List.get(position);
                category = savingsPansionTable1.getTitle();
                details = savingsPansionTable1.getData();
                break;
            case 22:
                SavingsFamilyTable1 savingsFamilyTable1 = savingsFamilyTable1List.get(position);
                category = savingsFamilyTable1.getTitle();
                details = savingsFamilyTable1.getData();
                break;
        }
        txtCategoryName.setText(category);
        txtDetails.setText(details);
    }

    private void setRequiredFielsForSavingsProduct(View adapterView, int position) {
        final TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        txtDMPInfo.setText(categoriesName[position]);
        txtDMPInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentService(txtDMPInfo.getText().toString());
            }
        });
    }

    public void startIntentService(String data) {
        Intent intent = new Intent(context, FragmentActivity.class);
        intent.putExtra("Fragment Name", data);
        context.startActivity(intent);
    }

    private void setRequierdFieldsForKeyFeaturesList(View adapterView, int position) {
        TextView txtNameOFSchemes = (TextView) adapterView.findViewById(R.id.txtNameOfSchemes);
        TextView txtMaturity = (TextView) adapterView.findViewById(R.id.txtMaturity);
        TextView txtLimit = (TextView) adapterView.findViewById(R.id.txtLimit);
        TextView txtInterestRate = (TextView) adapterView.findViewById(R.id.txtInterestRate);
        TextView txtTaxTreatment = (TextView) adapterView.findViewById(R.id.txtTaxTreatment);
        TextView txtInvestors = (TextView) adapterView.findViewById(R.id.txtInvestors);
        TextView txtPenalty = (TextView) adapterView.findViewById(R.id.txtPenalty);
        TextView txtImplementingAgencies = (TextView) adapterView
                .findViewById(R.id.txtImplementingAgencies);
        TextView txtComments = (TextView) adapterView.findViewById(R.id.txtComments);
        KeyFeatures keyFeatures = keyFeaturesList.get(position);
        if (keyFeatures != null) {
            txtNameOFSchemes.setText(getStringData(keyFeatures.getNameOfSchemes()));
            txtMaturity.setText(getStringData(keyFeatures.getMaturity()));
            txtLimit.setText(getStringData(keyFeatures.getLimit()));
            txtInterestRate.setText(getStringData(keyFeatures.getInterestRate()));
            txtTaxTreatment.setText(getStringData(keyFeatures.getTaxTreatment()));
            txtInvestors.setText(getStringData(keyFeatures.getInvestors()));
            txtPenalty.setText(getStringData(keyFeatures.getPenalty()));
            txtImplementingAgencies.setText(keyFeatures.getImplementingAgency());
            txtComments.setText(keyFeatures.getComments());
        }
    }

    private void setRequierdFieldsForContactUsSpcialBureu(View adapterView, int position) {
        TextView labelOffice = (TextView) adapterView.findViewById(R.id.labelName);
        TextView labelDesignation = (TextView) adapterView.findViewById(R.id.labelDesignation);
        TextView labelPhone = (TextView) adapterView.findViewById(R.id.labelPhone);
        TextView txtOffice = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtDesignation);
        TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtPhone);
        ContactUsSpecialBureau contactUsSpecialBureau = contactUsSpecialBureauList.get(position);
        if (contactUsSpecialBureau != null) {
            labelOffice.setText("Office:");
            labelDesignation.setText("Designation");
            labelPhone.setText("Phone:");
            txtOffice.setText(getStringData(contactUsSpecialBureau.getNameOfTheOffices()));
            txtPhone.setText(getStringData(contactUsSpecialBureau.getTelePhoneNumber()));
            txtDesignation.setText(getStringData(contactUsSpecialBureau.getContactPerson()));
        }
    }

    private void setRequiredFieldsForContactUsRegionalOffices(View adapterView, int position) {
        TextView labelOffice = (TextView) adapterView.findViewById(R.id.labelName);
        TextView labelDesignation = (TextView) adapterView.findViewById(R.id.labelDesignation);
        TextView labelPhone = (TextView) adapterView.findViewById(R.id.labelPhone);
        TextView txtOffice = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtDesignation);
        TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtPhone);
        ContactUsRegionalOffice contactUsRegionalOffice = contactUsRegionalOfficeList.get(position);
        if (contactUsRegionalOffice != null) {
            labelOffice.setText("Office:");
            labelDesignation.setText("Designation");
            labelPhone.setText("Phone:");
            txtOffice.setText(getStringData(contactUsRegionalOffice.getNameOfTheOffice()));
            txtPhone.setText(getStringData(contactUsRegionalOffice.getTelephone()));
            txtDesignation.setText(getStringData(contactUsRegionalOffice.getDesignation()));
        }
    }

    private void setRequiredFieldsForContactUsHeadOffice(View adapterView, int position) {
        TextView labelDesignation = (TextView) adapterView.findViewById(R.id.labelName);
        TextView labelPhone = (TextView) adapterView.findViewById(R.id.labelDesignation);
        TextView labelEmail = (TextView) adapterView.findViewById(R.id.labelPhone);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtDesignation);
        TextView txtEmail = (TextView) adapterView.findViewById(R.id.txtPhone);
        ContactUsHeadOffice contactUsHeadOffice = contactUsHeadOfficeList.get(position);
        if (contactUsHeadOffice != null) {
            labelDesignation.setText("Designation:");
            labelPhone.setText("Phone:");
            labelEmail.setText("Email");
            txtDesignation.setText(getStringData(contactUsHeadOffice.getDesignation()));
            txtEmail.setText(getStringData(contactUsHeadOffice.getEmailAddress()));
            txtPhone.setText(getStringData(contactUsHeadOffice.getPhoneNumber()));
        }
    }

    private String getStringData(String data) {
        String res = null;
        if (data != null && !data.equals("null") && !data.equals("") && !data.equals("নাই") &&
                !data.equalsIgnoreCase("Nil")) {
            res = data;
        }
        return res;
    }

    private void setRequiredFieldsForAgencies(View adapterView, int position) {
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.txt_dmp_info);
        txtDMPInfo.setText(organogramAssociateAgenciesList.get(position).getData());
    }

    private void setRequierdFieldsForCharterDuites(View adapterView, int position) {
        OrganogramChaterOfDuites organogramChaterOfDuites = organogramChaterOfDuitesList
                .get(position);
        TextView txtCategoryName = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDetails = (TextView) adapterView.findViewById(R.id.txt_details);
        txtCategoryName.setText(organogramChaterOfDuites.getTitle());
        txtDetails.setText(organogramChaterOfDuites.getData());
    }

    private void setRequiredFieldsForForms(View adapterView, int position) {
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        txtDMPInfo.setText(formsList.get(position).getTitle());
    }

   /* private void setRequierdFieldsForComplaintNumbers(View adapterView, int position) {
        TextView textView = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        textView.setText(complaintTableList.get(position).getPhNumber());
    }

    private void setRequiredFieldsForDMPPress(View adapterView, int position) {
        TextView txtCategoryName = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDetails = (TextView) adapterView.findViewById(R.id.txt_details);
        txtCategoryName.setText(dmpPressTableList.get(position).getHeadline());
        txtDetails.setText("তারিখ: " + dmpPressTableList.get(position).getDate());
    }


    private void setImageView(ImageView imageView, String imageLink) {
        Picasso.with(context).load(imageLink).into(imageView);
    }

    private void setRequiredFieldsForImportantNumbers(View adapterView, int position) {
        ImageView imageView = (ImageView) adapterView.findViewById(R.id.imageView);
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        if (LIST_VIEW_SHOW_TYPE == 5) {
            imageView.setImageResource(R.drawable.ic_action_ic_action_error);
        } else {
            imageView.setImageResource(R.drawable.ic_action_ic_action_help);
        }
        txtDMPInfo.setText(importantNumbersTableList.get(position).getNumber());
    }

    private void setRequierdFieldsForWeRemember(View adapterView, int position) {
        ImageView imageView = (ImageView) adapterView.findViewById(R.id.imageView);
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        imageView.setImageResource(images[position]);
        txtDMPInfo.setText(weRememberTables.get(position).getName());
    }*/

    private void setRequierdFieldsForDMPPartners(View adapterView, int position) {
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.txt_dmp_info);
        txtDMPInfo.setText(aboutUsList.get(position).getData());
    }

    private void setRequiredFieldsForNewInitiatives(View adapterView, int position) {
        TextView txtDMPInfo = (TextView) adapterView.findViewById(R.id.dmp_text_info);
        txtDMPInfo.setText(introductionPdfList.get(position).getTitle());
    }

    private void setRequierdFieldsForDMPServices(View adapterView, int position) {
        IntroductionTable introductionTable = introductionTableList.get(position);
        TextView txtCategoryName = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDetails = (TextView) adapterView.findViewById(R.id.txt_details);
        txtCategoryName.setText(introductionTable.getTitle());
        txtDetails.setText(introductionTable.getData());
    }
}
