package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "about_us")
public class AboutUs extends IntroductionTable implements Serializable{

    public AboutUs() {
        super();
    }

    public AboutUs(String title, String data) {
        super(title, data);
    }

    public static List<AboutUs> getAboutUsList(String title) {
        List<AboutUs> aboutUsList = new Select().from(AboutUs.class).
                where("title = ?", new String[]{title})
                .execute();
        return aboutUsList;
    }
}
