package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "introduction_pdf")
public class IntroductionPdf extends IntroductionTable implements Serializable{

    @Column(name = "pdf")
    private String pdf;

    public IntroductionPdf() {
        super();
    }

    public IntroductionPdf(String title, String pdf) {
        super(title);
        this.pdf = pdf;
        this.save();
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public static List<IntroductionPdf> getIntroductionPdfList() {
        List<IntroductionPdf> introductionPdfList = new Select()
                .from(IntroductionPdf.class)
                .execute();
        return introductionPdfList;
    }
}
