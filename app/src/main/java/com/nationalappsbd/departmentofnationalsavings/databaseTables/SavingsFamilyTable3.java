package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_family_table3")
public class SavingsFamilyTable3 extends Model {

    @Column(name = "Investment_amount")
    private String investmentAmount;

    @Column(name = "Amount__of_Profit")
    private String amountOfProfit;

    @Column(name = "Amount_of_SSP")
    private String amountOfSSP;

    @Column(name = "Amount_of_Total_profit")
    private String amountOfTotalProfit;


    public SavingsFamilyTable3() {
        super();
    }

    public SavingsFamilyTable3(String investmentAmount, String amountOfProfit,
                               String amountOfSSP, String amountOfTotalProfit) {
        this.investmentAmount = investmentAmount;
        this.amountOfProfit = amountOfProfit;
        this.amountOfSSP = amountOfSSP;
        this.amountOfTotalProfit = amountOfTotalProfit;
        this.save();
    }

    public String getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(String investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public String getAmountOfProfit() {
        return amountOfProfit;
    }

    public void setAmountOfProfit(String amountOfProfit) {
        this.amountOfProfit = amountOfProfit;
    }

    public String getAmountOfSSP() {
        return amountOfSSP;
    }

    public void setAmountOfSSP(String amountOfSSP) {
        this.amountOfSSP = amountOfSSP;
    }

    public String getAmountOfTotalProfit() {
        return amountOfTotalProfit;
    }

    public void setAmountOfTotalProfit(String amountOfTotalProfit) {
        this.amountOfTotalProfit = amountOfTotalProfit;
    }

    public static List<SavingsFamilyTable3> getFamilyTable3List() {
        List<SavingsFamilyTable3> savingsFamilyTable3List = new Select()
                .from(SavingsFamilyTable3.class)
                .execute();
        return savingsFamilyTable3List;
    }

}
