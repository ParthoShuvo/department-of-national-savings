package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_3_month_table_2")
public class Savings3MonthsTable2 extends Model {

    @Column(name = "Time period")
    private String timeOfPeriod;

    @Column(name = "Rate of\n" +
            "profit")
    private String rateOfProfit;

    @Column(name = "SSP")
    private String SSP;

    @Column(name = "Total Rate")
    private String totalRate;

    @Column(name = "Payable Profit")
    private String payableWithProfitPer100;

    public Savings3MonthsTable2() {
        super();
    }

    public Savings3MonthsTable2(String timeOfPeriod, String rateOfProfit,
                                String SSP, String totalRate, String payableWithProfitPer100) {
        this.timeOfPeriod = timeOfPeriod;
        this.rateOfProfit = rateOfProfit;
        this.SSP = SSP;
        this.totalRate = totalRate;
        this.payableWithProfitPer100 = payableWithProfitPer100;
    }

    public static List<Savings3MonthsTable2> getSavings3MonthsTable2List() {
        List<Savings3MonthsTable2> savings3MonthsTable2List = new Select()
                .from(Savings3MonthsTable2.class)
                .execute();
        return savings3MonthsTable2List;
    }

    public String getTimeOfPeriod() {
        return timeOfPeriod;
    }

    public void setTimeOfPeriod(String timeOfPeriod) {
        this.timeOfPeriod = timeOfPeriod;
    }

    public String getRateOfProfit() {
        return rateOfProfit;
    }

    public void setRateOfProfit(String rateOfProfit) {
        this.rateOfProfit = rateOfProfit;
    }

    public String getSSP() {
        return SSP;
    }

    public void setSSP(String SSP) {
        this.SSP = SSP;
    }

    public String getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(String totalRate) {
        this.totalRate = totalRate;
    }

    public String getPayableWithProfitPer100() {
        return payableWithProfitPer100;
    }

    public void setPayableWithProfitPer100(String payableWithProfitPer100) {
        this.payableWithProfitPer100 = payableWithProfitPer100;
    }
}
