package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "contact_us_regional_office")
public class ContactUsRegionalOffice extends Model {

    @Column(name = "Name of the Office")
    private String nameOfTheOffice;

    @Column(name = "Designation")
    private String designation;

    @Column(name = "Telephone Number")
    private String telephone;


    public ContactUsRegionalOffice() {
        super();
    }

    public ContactUsRegionalOffice(String nameOfTheOffice, String designation, String telephone) {
        super();
        this.nameOfTheOffice = nameOfTheOffice;
        this.designation = designation;
        this.telephone = telephone;
        this.save();
    }

    public ContactUsRegionalOffice(String designation) {
        super();
        this.designation = designation;
        this.save();
    }

    public String getNameOfTheOffice() {
        return nameOfTheOffice;
    }

    public void setNameOfTheOffice(String nameOfTheOffice) {
        this.nameOfTheOffice = nameOfTheOffice;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public static List<ContactUsRegionalOffice> getContactUsRegionalOfficeList() {
        List<ContactUsRegionalOffice> contactUsRegionalOfficeList = new Select()
                .from(ContactUsRegionalOffice.class)
                .execute();
        return contactUsRegionalOfficeList;
    }
}
