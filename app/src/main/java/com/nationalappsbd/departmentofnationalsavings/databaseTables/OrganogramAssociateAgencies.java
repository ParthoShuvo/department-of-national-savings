package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "organogram_associate_agencies")
public class OrganogramAssociateAgencies extends IntroductionTable implements Serializable{

    public OrganogramAssociateAgencies() {
        super();
    }

    public OrganogramAssociateAgencies(String title, String data) {
        super(title, data);
    }

    public static List<OrganogramAssociateAgencies> getOrganogramAssociateAgenciesList() {
        List<OrganogramAssociateAgencies> organogramAssociateAgenciesList = new Select()
                .from(OrganogramAssociateAgencies.class)
                .execute();
        return organogramAssociateAgenciesList;
    }
}
