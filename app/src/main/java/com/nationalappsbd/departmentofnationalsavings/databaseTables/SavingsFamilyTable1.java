package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/8/15.
 */

@Table(name = "SavingFamilyTable1")
public class SavingsFamilyTable1 extends IntroductionTable {

    public SavingsFamilyTable1() {
        super();
    }

    public SavingsFamilyTable1(String title, String data) {
        super(title, data);
        this.save();
    }

    public static List<SavingsFamilyTable1> getSavingsFamilyTable1List() {
        List<SavingsFamilyTable1> savingsFamilyTable1List = new Select()
                .from(SavingsFamilyTable1.class).execute();
        return savingsFamilyTable1List;
    }

}
