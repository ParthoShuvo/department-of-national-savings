package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_3_month_table_3")
public class Savings3MonthsTable3 extends Model {

    @Column(name = "Investment_amount")
    private String investmentAmount;

    @Column(name = "Amount__of_Profit")
    private String amountOfProfit;

    @Column(name = "Amount_of_SSP")
    private String amountOfSSP;

    @Column(name = "Amount_of_Total_profit")
    private String amountOfTotalProfit;


    public Savings3MonthsTable3() {
        super();
    }

    public Savings3MonthsTable3(String investmentAmount, String amountOfProfit,
                                String amountOfSSP, String amountOfTotalProfit) {
        super();
        this.investmentAmount = investmentAmount;
        this.amountOfProfit = amountOfProfit;
        this.amountOfSSP = amountOfSSP;
        this.amountOfTotalProfit = amountOfTotalProfit;
        this.save();
    }

    public String getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(String investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public String getAmountOfProfit() {
        return amountOfProfit;
    }

    public void setAmountOfProfit(String amountOfProfit) {
        this.amountOfProfit = amountOfProfit;
    }

    public String getAmountOfSSP() {
        return amountOfSSP;
    }

    public void setAmountOfSSP(String amountOfSSP) {
        this.amountOfSSP = amountOfSSP;
    }

    public String getAmountOfTotalProfit() {
        return amountOfTotalProfit;
    }

    public void setAmountOfTotalProfit(String amountOfTotalProfit) {
        this.amountOfTotalProfit = amountOfTotalProfit;
    }

    public static List<Savings3MonthsTable3> getSavings3MonthsTable3List() {
        List<Savings3MonthsTable3> savings3MonthsTable3List = new Select()
                .from(Savings3MonthsTable3.class)
                .execute();
        return savings3MonthsTable3List;
    }
}
