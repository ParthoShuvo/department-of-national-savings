package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/8/15.
 */

@Table(name = "Saving3MonthTable1")
public class Savings3MonthsTable1 extends IntroductionTable {

    public Savings3MonthsTable1() {
        super();
    }

    public Savings3MonthsTable1(String title, String data) {
        super(title, data);
        this.save();
    }

    public static List<Savings3MonthsTable1> getSavings3MonthsTable1List() {
        List<Savings3MonthsTable1> savings3MonthsTable1List = new Select()
                .from(Savings3MonthsTable1.class)
                .execute();
        return savings3MonthsTable1List;
    }
}
