package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/8/15.
 */

@Table(name = "Saving5YearTable1")
public class Savings5YearsTable1 extends IntroductionTable {

    public Savings5YearsTable1() {
        super();
    }

    public Savings5YearsTable1(String title, String data) {
        super(title, data);
        this.save();
    }

    public static List<Savings5YearsTable1> savings5YearsTable1List() {
        List<Savings5YearsTable1> savings5YearsTable1List = new Select()
                .from(Savings5YearsTable1.class)
                .execute();
        return savings5YearsTable1List;
    }
}
