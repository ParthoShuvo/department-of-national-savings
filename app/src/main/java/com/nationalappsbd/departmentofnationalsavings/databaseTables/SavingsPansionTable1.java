package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/8/15.
 */

@Table(name = "saving_pensioner_table_1")
public class SavingsPansionTable1 extends Model {

    @Column(name = "title")
    private String title;

    @Column(name = "data")
    private String data;

    public SavingsPansionTable1() {
        super();
    }

    public SavingsPansionTable1(String title, String data) {
        super();
        this.title = title;
        this.data = data;
        this.save();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static List<SavingsPansionTable1> getSavingsPansionTable1List()
    {
        List<SavingsPansionTable1> savingsPansionTable1List = new Select()
                .from(SavingsPansionTable1.class)
                .execute();
        return getSavingsPansionTable1List();
    }
}
