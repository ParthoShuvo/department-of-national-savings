package com.nationalappsbd.departmentofnationalsavings.interfaces;

/**
 * Created by tanvirahmedkhan on 4/16/15.
 */
public interface JsonLinks {

    public static String menuList = "http://api.national500apps.com/index.php?r=apiMenu/Getmenu";
    public static String submenuList = "http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu";

    public static String getGallery = "http://api.national500apps.com/index.php?r=apiMenu/GetImage";
    public static String getVideo = "http://api.national500apps.com/index.php?r=apiMenu/Getvideo";
    public static String dmpPresslink =
            "api.national500apps.com/index.php?r=apiMenu/Getmenu&app_id={525}";
    public final String dmpImagesLink = "http://api.national500apps.com/index.php?r=apiMenu/GetImage&app_id={666}";
    public final String dmpCommissionerMessage = "http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu&app_id={319}";
    public final String dmpCommissionerBioGraphy = "http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu&app_id={320}";
    public final String dmpCommissionerImageLink = "http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu&app_id={321}";
    public final String mostWantedCriminalsINageLink = "http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu&app_id={323}";
    public final String undefinedBodiesImageLink = "\"http://api.national500apps.com/index.php?r=apiMenu/Getsubmenu&app_id={324}";
}
