package com.nationalappsbd.departmentofnationalsavings.adapters.fragmentAdapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Shuvojit Saha Shuvo on 2/4/2015.
 */
public class FragmentAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> fragmentArrayList;
    private String[] fragmentPagerTitle;

    public FragmentAdapter(FragmentManager fm, ArrayList<Fragment> fragmentArrayList,
                           String[] fragmentPagerTitle) {
        super(fm);
        if (fragmentArrayList != null && fragmentArrayList.size() > 0 &&
                fragmentPagerTitle != null) {
            this.fragmentPagerTitle = fragmentPagerTitle;
            this.fragmentArrayList = fragmentArrayList;
        }
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = fragmentArrayList.get(position);
        return fragment;
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String pagerTitle = fragmentPagerTitle[position];
        return pagerTitle;
    }
}
