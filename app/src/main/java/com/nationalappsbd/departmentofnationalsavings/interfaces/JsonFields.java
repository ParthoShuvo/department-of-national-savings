package com.nationalappsbd.departmentofnationalsavings.interfaces;

/**
 * Created by tanvirahmedkhan on 4/16/15.
 */
public interface JsonFields {

    public static final String menu = "Menu";
    // public static String oldValues="oldValues";
    public static final String menuId = "menu_id";
    public static final String menu_name = "menu_name";
    public static final String app_code = "app_code";
    public static final String app_id = "app_id";
    public static final String menu_type = "menu_type";
    public static final String CONTENT = "content";
    public static final String submenuavailable = "has_submenu";
    public static final String submenu_name = "submenu_name";
    public static final String submenu_id = "submenu_id";
    public static final String imagepath = "file_path";
    public static final String DESCRIPTION = "description";
    public static final String videourl = "video_url";
    public static final String ACTIVE = "active";
    public static final String GALLERY_IMAGES = "GalleryImages";
    public static final String GALLERY_ID = "gallery_id";
    public static final String GALLERY_TYPE = "gallery_type";

}
