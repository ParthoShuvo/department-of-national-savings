package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_5_year_table_2")
public class Savings5YearsTable2 extends Savings3MonthsTable2{



    public Savings5YearsTable2() {
        super();
    }

    public Savings5YearsTable2(String timeOfPeriod, String rateOfProfit, String SSP,
                               String totalRate, String payableWithProfitPer100) {
        super(timeOfPeriod, rateOfProfit, SSP, totalRate, payableWithProfitPer100);
        this.save();
    }


    public static List<Savings5YearsTable2> getSavings5YearsTable2List()
    {
        List<Savings5YearsTable2> savings5YearsTable2List = new Select()
                .from(Savings5YearsTable2.class)
                .execute();
        return savings5YearsTable2List;
    }
}
