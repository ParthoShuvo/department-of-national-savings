package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "introduction_table")
public class IntroductionTable extends Model implements Serializable{

    @Column(name = "title")
    private String title;

    @Column(name = "data")
    private String data;

    public IntroductionTable() {
        super();
    }

    public IntroductionTable(String title) {
        super();
        this.title = title;
        this.save();
    }

    public IntroductionTable(String title, String data) {
        super();
        this.title = title;
        this.data = data;
        this.save();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static List<IntroductionTable> getIntroductionTableList() {
        List<IntroductionTable> introductionTableList = new Select()
                .from(IntroductionTable.class)
                .execute();
        return introductionTableList;
    }
}
