package com.nationalappsbd.departmentofnationalsavings.interfaces;

/**
 * Created by shuvojit on 5/14/15.
 */
public interface MenuIDsInterface {

    public final static String ABOUT_DMP_ID = "422";

    public final static String HOME_PAGE_ID = "782";

    public final static String DMP_COMMISSIONER_ID = "785";

    public final static String DMP_INTERNAL_ORGANIZATIONS = "793";

    public final static String CRIME_INFO = "809";

    public final static String WE_REMEMBER_1 = "763";

    public final static String WE_REMEMBER_2 = "764";

    public final static String WE_REMEMBER_3 = "765";

    public final static String NEWS_UPDATE = "810";

    public final static String COMPLAIN = "551";

    public final static String EMERGENCY_HELP = "557";

    public final static String EXTORTION_HELP = "564";

    public final static String POLICE_STATIONS = "829";

    public final static String POLICE_CLEARANCE_CERTIFICATE = "1070";


}
