package com.nationalappsbd.departmentofnationalsavings.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.adapters.fragmentAdapters.FragmentAdapter;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable1;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable2;
import com.nationalappsbd.departmentofnationalsavings.databaseTables.SavingsPansionTable3;
import com.nationalappsbd.departmentofnationalsavings.interfaces.Initializer;
import com.nationalappsbd.departmentofnationalsavings.tabs.SlidingTabLayout;

import java.util.ArrayList;

/**
 * Created by shuvojit on 5/7/15.
 */
public class SlidingTabViewFragment extends Fragment implements Initializer {


    private String FRAGMENT_SHOW_TYPE = null;
    private Context context;
    private ArrayList<Fragment> fragmentArrayList;
    private View fragmentView;
    private SlidingTabLayout slidingTabLayout;
    private FragmentAdapter fragmentAdapter;
    private ViewPager viewPager;
    private String[] viewPagerNames;


    public static Fragment getNewInstance(final String MENU_NAME) {
        Bundle bundle = new Bundle();
        bundle.putString("FRAGMENT_SHOW_TYPE", MENU_NAME);
        SlidingTabViewFragment slidingTabViewFragment = new SlidingTabViewFragment();
        slidingTabViewFragment.setArguments(bundle);
        return slidingTabViewFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            context = getActivity();
            Bundle bundle = getArguments();
            FRAGMENT_SHOW_TYPE = bundle.getString("FRAGMENT_SHOW_TYPE");
            switch (FRAGMENT_SHOW_TYPE) {
                case "Introduction":
                    setFragmentForMainPage();
                    break;
                case "Organogram":
                    setFragmentForDmpUnits();
                    break;
                case "Saving Product":
                    setFragmentsForDmpCommissiner();
                    break;
                case "About Us":
                    setFragmentForAboutUs();
                    break;
                case "Contact Us":
                    setFragmentForContactUs();
                    break;
                case "5 Years Bangladesh Sanchayapatra":
                    setFragmentForyearsSanchaypatra();
                    break;
                case "3 monthly":
                    setFragmentForMonthlySanchapatra();
                    break;
                case "Pension":
                    setFragmnetForPansion();
                    break;
                case "Poribar sanchayapatra":
                    setFragmentForFamily();
                    break;
            }
        }
    }

    private void setFragmentForFamily() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(13, "Family 1");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(13, "Family 2");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(13, "Family 3");
        fragmentArrayList.add(fragment);
        viewPagerNames = new String[]{"Introduction", "Rate of profit",
                "Denomination-wise profit"};
    }

    private void setFragmnetForPansion() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        Log.e(getClass().getName(), "Pansion Found");
        Log.e(getClass().getName(), SavingsPansionTable2
                .getSavingsPansionTable2List().size() + "");
        Log.e(getClass().getName(), SavingsPansionTable3
                .getSavingsPansionTable3List().size() + "");
        fragment = DMPNecessaryInfoFragment.getNewInstance(12, "Pansion 2");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(12, "Pansion 3");
        fragmentArrayList.add(fragment);
        viewPagerNames = new String[]{"Rate of profit",
                "Denomination-wise profit"};
    }

    private void setFragmentForMonthlySanchapatra() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(10, "3 months 1");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(10, "3 months 2");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(10, "3 months 3");
        fragmentArrayList.add(fragment);
        viewPagerNames = new String[]{"Introduction", "Rate of profit",
                "Denomination-wise profit"};
    }

    private void setFragmentForyearsSanchaypatra() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(9, "5 years 1");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(9, "5 years 2");
        fragmentArrayList.add(fragment);
        viewPagerNames = new String[]{"Introduction", "Year-wise profit rate with SSP"};
    }

    private void setFragmentForContactUs() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(8, "Head Office");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(8, "Regional Office");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(8, "Special Bureau");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.contact_us);
    }

    private void setFragmentForAboutUs() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(2, "Formation");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(2, "Organizational Setup");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.aboutUs);
    }

    private void setFragmentForMainPage() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "Introduction_details");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "Introduction_pdf");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.mainPage);
    }

    private void setFragmentForCrimeINfo() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(4, "most wanted criminals");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(4, "undefined dead bodies");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.crimeInfo);

    }

    private void setFragmentsForDmpCommissiner() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(5, "Sanchayapatra");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(5, "Development Bond");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.savingsProduct);
    }

    private void setFragmentForDmpUnits() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(3, "CHARTER_OF_DUTIES");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(3, "Associate_Agencies");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.dmpUnits);


    }

    private void setFragmentsForAboutDMP() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "DMP SERVICES");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "New Initiatives");
        fragmentArrayList.add(fragment);
        fragment = DMPNecessaryInfoFragment.getNewInstance(1, "DMP Partners");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.aboutUs);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = null;

        if (savedInstanceState == null) {
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fragmentView = inflater.inflate(R.layout.view_pager_includer_fragment_layout,
                    container, false);
            initialize();
            setFragmentAdapter();
        }

        return fragmentView;
    }

    private void setFragmentAdapter() {
        if (viewPager != null && fragmentAdapter != null) {
            viewPager.setAdapter(fragmentAdapter);
            slidingTabLayout.setDistributeEvenly(true);
            slidingTabLayout.setViewPager(viewPager);
            slidingTabLayout.setDistributeEvenly(true);
            slidingTabLayout.setSelectedIndicatorColors(getResources()
                    .getColor(R.color.colorAccent));
        }
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            viewPager = (ViewPager) fragmentView.findViewById(R.id.scroll_view_pager);
            slidingTabLayout = (SlidingTabLayout) fragmentView.findViewById(R.id.slidingTab);
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null && fragmentArrayList != null && viewPagerNames != null) {
                fragmentAdapter = new FragmentAdapter(fragmentManager,
                        fragmentArrayList, viewPagerNames);
            }
        }
    }
}
