package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "contact_us_special_bureau")
public class ContactUsSpecialBureau extends Model {

    @Column(name = "Name of the offices")
    private String nameOfTheOffices;

    @Column(name = "Contact Person")
    private String contactPerson;

    @Column(name = "Telephone Number")
    private String telePhoneNumber;

    public ContactUsSpecialBureau() {
        super();
    }

    public ContactUsSpecialBureau(String nameOfTheOffices, String contactPerson,
                                  String telePhoneNumber) {
        super();
        this.nameOfTheOffices = nameOfTheOffices;
        this.contactPerson = contactPerson;
        this.telePhoneNumber = telePhoneNumber;
        this.save();
    }

    public static List<ContactUsSpecialBureau> getContactUsSpecialBureauList() {
        List<ContactUsSpecialBureau> contactUsSpecialBureauList = new Select()
                .from(ContactUsSpecialBureau.class)
                .execute();
        return contactUsSpecialBureauList;
    }

    public String getNameOfTheOffices() {
        return nameOfTheOffices;
    }

    public void setNameOfTheOffices(String nameOfTheOffices) {
        this.nameOfTheOffices = nameOfTheOffices;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getTelePhoneNumber() {
        return telePhoneNumber;
    }

    public void setTelePhoneNumber(String telePhoneNumber) {
        this.telePhoneNumber = telePhoneNumber;
    }
}
