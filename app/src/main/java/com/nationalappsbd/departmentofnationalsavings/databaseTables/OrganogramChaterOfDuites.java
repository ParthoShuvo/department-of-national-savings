package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 6/7/15.
 */

@Table(name = "organogram_chater_duites")
public class OrganogramChaterOfDuites extends IntroductionTable implements Serializable{

    public OrganogramChaterOfDuites() {
        super();
    }

    public OrganogramChaterOfDuites(String title, String data) {
        super(title, data);
    }

    public static List<OrganogramChaterOfDuites> getOrganogramChaterOfDuitesList() {
        List<OrganogramChaterOfDuites> organogramChaterOfDuitesList = new Select()
                .from(OrganogramChaterOfDuites.class)
                .execute();
        return organogramChaterOfDuitesList;
    }
}
