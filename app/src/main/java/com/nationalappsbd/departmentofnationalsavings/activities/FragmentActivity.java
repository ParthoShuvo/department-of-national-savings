package com.nationalappsbd.departmentofnationalsavings.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nationalappsbd.departmentofnationalsavings.R;
import com.nationalappsbd.departmentofnationalsavings.fragments.SlidingTabViewFragment;
import com.nationalappsbd.departmentofnationalsavings.interfaces.Initializer;

public class FragmentActivity extends ActionBarActivity implements Initializer{

    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setContentView(R.layout.frame_layout_container_activity);
            initialize();

        }
    }


    @Override
    public void initialize() {
        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        TextView toolbarTextView = (TextView) actionBarToolbar.findViewById(R.id.toolBarTextView);
        actionBarToolbar.setTitle("");
        Intent intent = getIntent();
        fragmentManager = getSupportFragmentManager();
        String fragmentName = intent.getStringExtra("Fragment Name");
        setFragment(fragmentName);
        toolbarTextView.setText(fragmentName);
        setSupportActionBar(actionBarToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    private void setFragment(String fragmentName) {
        Fragment fragment = null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Log.e(getClass().getName(), fragmentName);
        switch (fragmentName) {
            case "5 Years Bangladesh Sanchayapatra":
                Log.e(getClass().getName(), fragmentName);
                fragment = SlidingTabViewFragment
                        .getNewInstance("5 Years Bangladesh Sanchayapatra");
                break;
            case "3 monthly":
                Log.e(getClass().getName(), fragmentName);
                fragment = SlidingTabViewFragment
                        .getNewInstance("3 monthly");
                break;
            case "Pensioner Sanchayapatra":
                Log.e(getClass().getName(), fragmentName);
                fragment = SlidingTabViewFragment
                        .getNewInstance("Pension");
                break;
            case "Poribar sanchayapatra":
                Log.e(getClass().getName(), fragmentName);
                fragment = SlidingTabViewFragment
                        .getNewInstance("Poribar sanchayapatra");
                break;

           /* case "টেন্ডার ও কোটেশান":

                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(8, "Tender");
                break;
            case "কেন্দ্রীয় ই-সেবা":
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(13, "Central E Services");
                break;
            case "গুরুত্বপূর্ণ লিংক":
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(14, "Important Links");
                break;
            case "মানচিত্রে অবস্থান":
                PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo =
                        PrimaryEducationDepartmentInfo.getPrimaryEducationDepartmentInfo();
                fragment = PrimaryEducationDepartmentInGoogleMapFragment
                        .newInstance(primaryEducationDepartmentInfo,
                                primaryEducationDepartmentInfo.getLatitude(),
                                primaryEducationDepartmentInfo.getLongitude(),
                                10.5f);
                break;
            case "ওয়েব পেইজ":
                fragment = WebViewFragment.newInstance("http://www.dpe.gov.bd/");
                break;
            case "ওয়েব মেইল":
                fragment = WebViewFragment
                        .newInstance("https://mail.dpe.gov.bd/webmail/src/login.php");
                break;
            case "ফেইসবুক পেইজ":
                fragment = WebViewFragment
                        .newInstance("https://www.facebook.com/dpebd");
                break;
            case "খবর":
                NewsUpdate newsUpdate = (NewsUpdate) getIntent()
                        .getSerializableExtra("News Update");
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment.getNewInstance(15,
                        "News Update", newsUpdate);
                break;
            case "প্রাথমিক বিদ্যালয়":
                fragment = WebViewFragment
                        .newInstance(getIntent().getStringExtra("Web Url"));
                break;*/
            default:
                break;

        }
        if (fragment != null && fragmentManager != null
                && fragmentTransaction != null) {
            fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
        } else {
            Log.e(getClass().getName(), "Null Found");
        }
    }
}
