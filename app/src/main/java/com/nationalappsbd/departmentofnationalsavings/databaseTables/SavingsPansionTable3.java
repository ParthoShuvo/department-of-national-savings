package com.nationalappsbd.departmentofnationalsavings.databaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 6/9/15.
 */

@Table(name = "saving_pansion_table3")
public class SavingsPansionTable3 extends Savings3MonthsTable3 {
    public SavingsPansionTable3() {
        super();
    }

    public SavingsPansionTable3(String investmentAmount, String amountOfProfit,
                                String amountOfSSP, String amountOfTotalProfit) {
        super(investmentAmount, amountOfProfit, amountOfSSP, amountOfTotalProfit);
        this.save();
    }

    public static List<SavingsPansionTable3> getSavingsPansionTable3List() {
        List<SavingsPansionTable3> savingsPansionTable3List = new Select()
                .from(SavingsPansionTable3.class)
                .execute();
        return savingsPansionTable3List;
    }
}
