package com.nationalappsbd.departmentofnationalsavings.applications;

import android.app.Application;
import android.util.Log;

import com.activeandroid.ActiveAndroid;

/**
 * Created by shuvojit on 4/9/15.
 */
public class BootStrapper extends Application {

    public BootStrapper() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        Log.e(getClass().getName(), "Active Android has been initialize");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ActiveAndroid.dispose();
        Log.e(getClass().getName(), "Active Android has been disposed");
    }
}
